# TK 2 PPW - KB08

[![pipeline status](https://gitlab.com/tasyashbr/ppw-kb08-tk2/badges/master/pipeline.svg)](https://gitlab.com/tasyashbr/ppw-kb08-tk2/commits/master)
[![coverage report](https://gitlab.com/tasyashbr/ppw-kb08-tk2/badges/master/coverage.svg)](https://gitlab.com/tasyashbr/ppw-kb08-tk2/commits/master)

Link Heroku: https://citizi-id.herokuapp.com

# Informasi Branch dan Pembagian Fitur

**Branch**
*  adrian: Adrian Kaiser
*  tasput: Tasya Putri Shabira
*  mahdia: Mahdia Aliyya Nuha K
*  tolhas: Tolhas Parulian J

**Fitur**
*  [mainapp] Crowd tracker, register, and login: Adrian Kaiser
*  [add_hotel] Add hotel: Mahdia Aliyya Nuha K
*  [search_hotel] Search for booking hotel: Tasya Putri Shabira
*  [booking_hotel] Booking and booking details: Tolhas Parulian J

# Tentang Aplikasi

Aplikasi Citizi memberikan informasi mengenai jumlah populasi masyarakat di
suatu kota pada waktu tertentu dengan data yang nantinya (pada beberapa tahun yang akan datang)
dikumpulkan dari situs penyedia travel dan penginapan seperti traveloka.com, tiket.com,
pegipegi.com, dan lain-lain. Data-data yang dikumpulkan meliputi jumlah pendatang di suatu
kota pada jangka waktu tertentu untuk memperkirakan kepadatan kota tersebut.

Dengan adanya informasi yang tersedia, baik masyarakat umum maupun organisasi dan
pemerintah dapat memantau tingkat kepadatan suatu kota dengan tujuan masing-masing.
Untuk lebih lengkapnya dapat melihat persona website Citizi di [sini](https://docs.google.com).

# Fitur Aplikasi Citizi (current)
* Crowd tracking
* Add hotel (future update in 10 years: get data from another platform, so no need to fill the form manually)
* Search for hotel by city
* Book hotel
* See booking details
* Login feature (with authorization for basic users and companies)
* Hotel rating

# Future updates will include (untuk pemenuhan syarat ide dengan tema Industri 4.0 hehe):
* Integrasi aplikasi Citizi dengan platform penyedia data
* Notifikasi ketika terdapat lonjakan kepadatan pada beberapa hari mendatang berdasarkan analisis data dan GPS pengguna
* Visualisasi data yang lebih atraktif
* Prediksi dan suggestion untuk tiap pengguna
* Fitur user interface berbasis AI untuk get data pengguna
