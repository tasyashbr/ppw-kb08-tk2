from django import forms
from . import models


class addHotelForm (forms.Form):
    hotel_name = forms.CharField(
        max_length=50, required=True, widget=forms.TextInput(attrs={'class': 'form-hotel hotel-name', 'placeholder': 'Enter Hotel Name'}))
    city = forms.CharField(max_length=50, required=True, widget=forms.TextInput(
        attrs={'class': 'form-hotel hotel-city', 'placeholder': 'Select a city'}))
    address = forms.CharField(max_length=50, required=True, widget=forms.TextInput(
        attrs={'class': 'form-hotel hotel-address', 'placeholder': 'ex. Margonda Raya No. 273', 'label_tag': 'Hotel Address'}))
