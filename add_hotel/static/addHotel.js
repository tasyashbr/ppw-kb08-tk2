$(document).ready(function () {
    recent_added();
});

$("#addhotel-button").click(function (e) {
    $.ajax({
        type: 'POST',
        url: 'https://citizi-id.herokuapp.com/add_hotel/api/post/ ',
        contentType: "application/json",
        data: JSON.stringify({
            "hotel_name": $('#id_hotel_name').val(),
            "city": $('#id_city').val(),
            "address": $('#id_address').val(),
        }),
        success: function (json) {
            console.log(json);
            if (json.success) {
                $("#success-msg").html($('#id_hotel_name').val() + ' is added');
            }
            recent_added();
        },
        error: function (xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
});

function recent_added() {
    $.ajax({
        type: 'GET',
        url: 'https://citizi-id.herokuapp.com/add_hotel/api/json-data/ ',
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            console.log(data[0].fields);
            console.log(data.length);
            $('#recently-added').html('')
            var result = '<h3>recently added hotels: </h3   >';
            if (data.length >= 3) {
                var show_amount = data.length - 3;
            } else {
                var show_amount = 0;
            }
            console.log(show_amount);
            for (var i = data.length - 1; i >= show_amount; i--) {
                result += "<div><p>" + data[i].fields.hotel_name + "-" + data[i].fields.city + "</p></div> ";
            }
            $('#recently-added').append(result);

        },
        error: function (xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
}

$('#addhotel-button').mouseenter(function () {
    $(this).fadeOut(500);
    $(this).fadeIn(500);
});

$('#addhotel-button').mousedown(function () {
    $(this).css('background-color', 'grey');
});

$('#addhotel-button').mouseup(function () {
    $(this).css('background-color', '#007EFF');
});

$('#addhotel-button').mouseleave(function () {
    $(this).fadeOut(100);
    $(this).fadeIn(100);
});