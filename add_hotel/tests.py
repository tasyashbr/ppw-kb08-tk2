from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from .views import addHotel
from .models import Hotel
from .forms import addHotelForm

# Create your tests here.


class AddHotelTest(TestCase):
    def test_addhotel_url_is_exist(self):
        response = Client().get('/add_hotel/')
        self.assertEqual(response.status_code, 200)

    def test_addhotel_page_content(self):
        request = HttpRequest()
        response = addHotel(request)
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('Add hotel', html_response)

    def test_hotel_model_str(self):
        test_model = Hotel(hotel_name="Test Hotel",
                           city="Depok", address="Jl. Test")
        test_model.save()
        test_model = Hotel.objects.get(hotel_name="Test Hotel")
        test_string = str(test_model)
        self.assertEqual("Test Hotel", test_string)

    def test_is_using_addhotel_template(self):
        response = Client().get('/add_hotel/')
        self.assertTemplateUsed(response, 'addhotel.html')

    def test_is_using_addhotel_func(self):
        found = resolve('/add_hotel/')
        self.assertEqual(found.func, addHotel)

    def test_model_can_create_new_hotel(self):
        new_hotel = Hotel.objects.create(
            hotel_name="New Hotel", city="New City", address="New Address")
        counting_all_available_hotel = Hotel.objects.all().count()
        self.assertEqual(counting_all_available_hotel, 1)

    def test_addhotel_form(self):
        form_data = {'hotel_name': 'New Test Hotel',
                     'city': 'A City', 'address': 'somewhere'}
        form = addHotelForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_addhotel_json_data_response(self):
        self.response = Client().get('/add_hotel/api/json-data/')
        self.assertEqual(self.response.status_code, 200)

    def test_post_response(self):
        self.response = Client().get('/add_hotel/api/post/')
        self.assertEqual(self.response.status_code, 200)
