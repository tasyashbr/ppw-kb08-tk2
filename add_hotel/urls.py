from django.urls import path
from . import views

app_name = 'add_hotel'

urlpatterns = [
    path('', views.addHotel, name="addHotel"),
    path('api/post/', views.addHotel_post, name="addHotelPost"),
    path('api/json-data/', views.convert_to_json, name="json-data"),
]
