from django.shortcuts import render, redirect, HttpResponse
from .forms import addHotelForm
from .models import Hotel
from django.core import serializers
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json

# Create your views here.


def convert_to_json(request):
    hotel_json = serializers.serialize("json", Hotel.objects.all())
    return HttpResponse(hotel_json, content_type='application/json')


def addHotel(request):
    print(request)
    response_data = {}

    form = addHotelForm()

    context = {
        'form': form,
    }
    return render(request, "addhotel.html", context)


@csrf_exempt
def addHotel_post(request):
    response_data = {}
    response_data['success'] = False
    if request.method == 'POST':
        request_data = json.loads(request.body)
        if request_data["hotel_name"] == "" or request_data["city"] == "" or request_data["address"] == "":
            return JsonResponse(response_data)
        hotel = Hotel(
            hotel_name=request_data["hotel_name"],
            city=request_data["city"],
            address=request_data["address"],
        )
        hotel.save()
        response_data['success'] = True
    return JsonResponse(response_data)
