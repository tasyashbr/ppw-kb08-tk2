from django.apps import AppConfig


class BookingHotelConfig(AppConfig):
    name = 'booking_hotel'
