from django.db import models
from datetime import datetime, date
from django.utils import timezone
from add_hotel.models import Hotel

class Booking(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    customer_name = models.CharField(max_length = 27, default = "User")
    check_in = models.DateField()
    check_out = models.DateField()
    guests = models.PositiveIntegerField()
    def __str__(self):
        return self.hotel.hotel_name + " booked by " + self.customer_name

