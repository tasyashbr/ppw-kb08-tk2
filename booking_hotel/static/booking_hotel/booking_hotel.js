var postOrder = (input) => {
    $.ajax({
        type:'POST',
        url: input,
        data:{
            customer_name:$('#id_customer_name').val(),
            guests:$('#id_guests').val(),
            check_in:$('#id_check_in').val(),
            check_out:$('#id_check_out').val(),
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
            action: 'post'
        },
        success:function(json){
            $(".errorList").empty();
            if (json.error == "null"){
                confirm("Pemesanan hotel berhasil");
                location.href = "/";
            } else {
                $(".errorList").empty();
                let error = json.error[0];
                console.log(error);
                for (let i = 1; i<error.length; i++){
                    $(".errorList").append(`<li>${error[i]}</li>`);
                }
            }
        },
        error : function(xhr,errmsg,err) {
        console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
    }
    });
};