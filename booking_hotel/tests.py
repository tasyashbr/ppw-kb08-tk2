from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import book_now
from .models import Booking
from .forms import BookingForm
from add_hotel.models import Hotel 
from django.utils import timezone
from django.apps import apps
from .apps import BookingHotelConfig
import datetime

class BookingHotelTest(TestCase):

    def test_booking_hotel_url_is_exist(self):
        new_hotel = Hotel(hotel_name = "Hotel Testing", city = "Surabaya", address = "Jalan Surabaya 1")
        new_hotel.save()
        new_booking = Booking(hotel = new_hotel, customer_name = "Agus", check_in = timezone.now(), check_out = timezone.now() + datetime.timedelta(days = 1))
        url = reverse('booking_hotel:book_now', args=(1,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_booking_hotel_using_template_and_exist(self):
        new_hotel = Hotel(hotel_name = "Hotel Testing", city = "Surabaya", address = "Jalan Surabaya 1")
        new_hotel.save()
        new_booking = Booking(hotel = new_hotel, customer_name = "Agus", check_in = timezone.now(), check_out = timezone.now() + datetime.timedelta(days = 1))
        url = reverse('booking_hotel:book_now', args=(1,))
        response = self.client.get(url)
        html_response = response.content.decode('utf8')
        self.assertTemplateUsed(response, 'booking_hotel/book_hotel_detail.html')    
        self.assertTrue(len(html_response) > 0)

    # def test_process_booking_hotel_using_template(self):
    #     new_hotel = Hotel(hotel_name = "Hotel Testing", city = "Surabaya", address = "Jalan Surabaya 1")
    #     new_hotel.save()
    #     new_booking = Booking(hotel = new_hotel, customer_name = "Agus", check_in = timezone.now(), check_out = timezone.now() + datetime.timedelta(days = 1))
    #     url = reverse('booking_hotel:book_process', args=(1,))
    #     response = self.client.get(url)
    #     html_response = response.content.decode('utf8')
    #     self.assertTrue(response, 'booking_hotel/book_hotel_detail.html')    

    def test_booking_hotel_using_book_now_func(self):
        url = reverse('booking_hotel:book_now', args=(1,))
        found = resolve(url)
        self.assertEqual(found.func, book_now)

    def test_model_can_create_new_booking(self):
        new_hotel = Hotel(hotel_name = "Hotel Testing", city = "Surabaya", address = "Jalan Surabaya 1")
        new_hotel.save()
        new_booking = Booking(hotel = new_hotel, customer_name = "Agus", check_in = timezone.now(), check_out = timezone.now() + datetime.timedelta(days = 1))
        counting_all_available_hotel = Hotel.objects.count()
        counting_all_available_booking = Hotel.objects.count()
        self.assertEqual(new_booking.__str__(), "Hotel Testing booked by Agus")
        self.assertEqual(counting_all_available_hotel, 1)
        self.assertEqual(counting_all_available_booking, 1)

    def test_booking_hotel_valid_input_forms_to_model(self):
        new_hotel = Hotel(hotel_name = "Hotel Testing", city = "Surabaya", address = "Jalan Surabaya 1")
        new_hotel.save()
        check_in = timezone.now()
        check_out = timezone.now() + datetime.timedelta(days = 1)
        form = BookingForm({'customer_name': 'Agus', 'check_in': check_in, 'check_out': check_out, 'guests': 2})
        self.assertTrue(form.is_valid())
        new_booking = Booking(hotel = new_hotel, customer_name = form['customer_name'].value(), check_in = form['check_in'].value(), check_out = form['check_out'].value(), guests = form['guests'].value())
        new_booking.save()
        self.assertEqual(new_booking.hotel, new_hotel)
        self.assertEqual(new_booking.customer_name, 'Agus')

    def test_booking_hotel_apps(self):
        self.assertEqual(BookingHotelConfig.name, 'booking_hotel')
        self.assertEqual(apps.get_app_config('booking_hotel').name, 'booking_hotel')

    def test_booking_hotel_forms(self):
        new_hotel = Hotel(hotel_name = "Hotel Testing", city = "Surabaya", address = "Jalan Surabaya 1")
        new_hotel.save()
        id_hotel = new_hotel.id
        check_in = timezone.now()
        check_out = timezone.now() + datetime.timedelta(days = 1)
        form = BookingForm({'customer_name': 'Agus', 'check_in': check_in, 'check_out': check_out, 'guests': 2})
        url = reverse('booking_hotel:book_now', args=(id_hotel,))
        response = self.client.post(url, {
            'customer_name': 'Agus',
            'check_in': check_in,
            'check_out': check_out,
            'guests': 2
        })
        formByClient = BookingForm(self.client.post(url, {
            'customer_name': 'Agus',
            'check_in': check_in,
            'check_out': check_out,
            'guests': "2"
        }))
        # print(formByClient.is_valid())
        self.assertEqual(response.status_code, 200)
        # self.assertEqual(response.status_code, 200)
        # self.assertEqual(response.method, "POST")



    # def test_landing_page_form_validation_for_blank_items(self):
    #     form = Booking(data = {'status': ''})
    #     self.assertFalse(form.is_valid())
    #     self.assertEqual(form.errors['status'], ["Status field cannot blank"])

    
    




# class BookingHotelTest(TestCase):

#     def test_booking_hotel_url_is_exist(self):
#         new_hotel = Hotel(hotel_name = "Hotel Testing", city = "Surabaya", address = "Jalan Surabaya 1")
#         new_hotel.save()
#         new_booking = Booking(hotel = new_hotel, customer_name = "Agus", check_in = timezone.now(), check_out = timezone.now() + datetime.timedelta(days = 1))
#         url = reverse('booking_hotel:book_now', args=(1,))
#         response = self.client.get(url)
#         self.assertEqual(response.status_code, 200)

#     def test_booking_hotel_using_template_and_exist(self):
#         new_hotel = Hotel(hotel_name = "Hotel Testing", city = "Surabaya", address = "Jalan Surabaya 1")
#         new_hotel.save()
#         new_booking = Booking(hotel = new_hotel, customer_name = "Agus", check_in = timezone.now(), check_out = timezone.now() + datetime.timedelta(days = 1))
#         url = reverse('booking_hotel:book_now', args=(1,))
#         response = self.client.get(url)
#         html_response = response.content.decode('utf8')
#         self.assertTemplateUsed(response, 'booking_hotel/book_hotel_detail.html')    
#         self.assertTrue(len(html_response) > 0)

#     def test_process_booking_hotel_using_template(self):
#         new_hotel = Hotel(hotel_name = "Hotel Testing", city = "Surabaya", address = "Jalan Surabaya 1")
#         new_hotel.save()
#         new_booking = Booking(hotel = new_hotel, customer_name = "Agus", check_in = timezone.now(), check_out = timezone.now() + datetime.timedelta(days = 1))
#         url = reverse('booking_hotel:book_process', args=(1,))
#         response = self.client.get(url)
#         html_response = response.content.decode('utf8')
#         self.assertTrue(response, 'booking_hotel/book_hotel_detail.html')    

#     def test_booking_hotel_using_index_func(self):
#         url = reverse('booking_hotel:book_now', args=(1,))
#         found = resolve(url)
#         self.assertEqual(found.func, book_now)

#     def test_model_can_create_new_booking(self):
#         new_hotel = Hotel(hotel_name = "Hotel Testing", city = "Surabaya", address = "Jalan Surabaya 1")
#         new_hotel.save()
#         new_booking = Booking(hotel = new_hotel, customer_name = "Agus", check_in = timezone.now(), check_out = timezone.now() + datetime.timedelta(days = 1))
#         counting_all_available_hotel = Hotel.objects.count()
#         counting_all_available_booking = Hotel.objects.count()
#         self.assertEqual(counting_all_available_hotel, 1)
#         self.assertEqual(counting_all_available_booking, 1)

