from django.urls import path
from . import views

app_name = 'booking_hotel'

urlpatterns = [
    path('<int:hotel_id>/', views.book_now, name = "book_now"),
    path('<int:hotel_id>/process', views.book_process, name = "book_process"),
]