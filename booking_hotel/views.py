from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .forms import BookingForm
from .models import Booking
from add_hotel.models import Hotel
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse

response = {}

def book_now(request, hotel_id):
    hotel = get_object_or_404(Hotel, pk = hotel_id)
    response['hotel'] = hotel
    response['BookingForm'] = BookingForm
    response['isFilled'] = ""
    return render(request, 'booking_hotel/book_hotel_detail.html', response)

@csrf_protect
def book_process(request, hotel_id):
    form = BookingForm(request.POST or None)
    print("wowwowo")
    # data_hotel = request.GET.get(hotel_id, None)
    hotel = get_object_or_404(Hotel, pk = hotel_id)
    response['hotel'] = hotel
    if request.POST.get('action') == 'post' and form.is_valid():
        print ("aku terpanggil")
        print(form.is_valid())
        booking = Booking(hotel = hotel,
        customer_name = request.POST['customer_name'],
        check_in = request.POST['check_in'],
        check_out = request.POST['check_out'],
        guests = request.POST['guests'])
        booking.save()
        data = {
            'data' : 'true',
            'error': "null",
            # 'booking': booking
        }
        return JsonResponse(data)
    else:
        data = {
            'data' : 'false',
            'error' : [(k, v[0]) for k, v in form.errors.items()],
            # 'booking': "null"
        }
        response['BookingForm'] = form
        return JsonResponse(data)

def index():
    return HttpResponse("haihai")
