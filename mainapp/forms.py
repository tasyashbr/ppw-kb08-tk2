from django import forms
from django.forms import ModelForm
from .models import CrowdTracker

class CreateCrowdTracker(ModelForm):
	class Meta:
		model = CrowdTracker
		fields = ['city', 'month', 'year']

		
		widgets = {
		'city' : forms.TextInput(attrs={'id':'city-form', 'class' : 'form-control mt-2', 'placeholder':'Search by city'}),
		'month': forms.Select(attrs={'id':'month-form', 'class':'form-control mb-2', 'placeholder':'Select Month'}), 
		'year': forms.TextInput(attrs={'id':'year-form', 'class':'form-control mb-2', 'placeholder':'Select Year'})
		}

	def __init__(self, *args, **kwargs):
		super(CreateCrowdTracker, self).__init__(*args, **kwargs)