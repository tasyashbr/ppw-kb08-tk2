from django.db import models

# Create your models here.
class CrowdTracker(models.Model):
	MONTHS = (
		(1,'January'),
		(2,'February'),
		(3,'March'),
		(4,'April'),
		(5,'May'),
		(6,'June'),
		(7,'July'),
		(8,'August'),
		(9,'September'),
		(10,'October'),
		(11,'November'),
		(12,'December'),
	)

	city = models.CharField(max_length=50)
	month = models.IntegerField(choices=MONTHS)
	year = models.IntegerField()

class DateVisitor(models.Model):
	date = models.DateField(primary_key=True)
	visitor = models.IntegerField()
	parent = models.ForeignKey(CrowdTracker, on_delete=models.CASCADE)