from django.test import TestCase, Client
from django.http import HttpRequest
from .views import *
from .models import *
from .forms import *

# Create your tests here.
class PageTest(TestCase):
	def test_if_Crowd_Tracker_url_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'crowd-tracker.html')

	def test_if_Display_url_exist(self):
		response = Client().get('/display/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'view-crowd.html')

	def test_login_page_exist(self):
		response = Client().get('/login/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'registration/login.html')

	def test_logout_page_exist(self):
		response = Client().get('/logout/')
		self.assertEqual(response.status_code, 302)

	def test_register_page_exist(self):
		response = Client().get('/signup/')
		self.assertEqual(response.status_code, 200)

	def test_Crowd_Tracker_Page(self):
		request = HttpRequest()
		response = crowdTracker(request)
		html_response = response.content.decode('utf8')
		self.assertIsNotNone(html_response)
		self.assertIn('Crowd Tracker', html_response)

	def test_view_crowd_Page(self):
		request = HttpRequest()
		response = displayByCity(request)
		html_response = response.content.decode('utf8')
		self.assertIsNotNone(html_response)
		self.assertIn('Citizi', html_response)

class CrowdTrackerTest(TestCase):
	def test_form_is_correct(self):
		form_data1 = {'city':'Hellsalem lot', 'month':7, 'year':2000}
		form_data2 = {'city':'This is an example of the wrong format because its city length is waaaaay above 50 what the hell', 'month':7, 'year':2000}
		form1 = CreateCrowdTracker(data=form_data1)
		form2 = CreateCrowdTracker(data=form_data2)
		self.assertTrue(form1.is_valid())
		self.assertFalse(form2.is_valid())

	def test_form_submit(self):
		response = Client().post('/display/',{'city':'Hellsalem lot', 'month':7, 'year':2000})
		model = CrowdTracker.objects.get(city='Hellsalem lot')
		self.assertEqual(model.city,'Hellsalem lot')
		self.assertEqual(response.status_code, 200)

	def test_form_submitted_is_displayed(self):
		response = Client().post('/display/',{'city':'Hellsalem lot', 'month':7, 'year':2000})
		html_response = response.content.decode('utf8')
		self.assertIn('Hellsalem lot', html_response)
		self.assertIn('July', html_response)
		self.assertIn('2000', html_response)