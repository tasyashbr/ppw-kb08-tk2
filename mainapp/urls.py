from django.urls import path
from . import views

app_name = 'mainapp'

urlpatterns = [
	path('secret/', views.placeholder, name="placeholder"),
	path('', views.crowdTracker, name="crowdTracker"),
	path('display/', views.displayByCity, name="displayByCity"),
	path('signup/', views.SignUp.as_view(), name='signup'),
]