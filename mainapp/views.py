from django.shortcuts import render
from .forms import CreateCrowdTracker
from .models import CrowdTracker, DateVisitor
from booking_hotel.models import Booking
from add_hotel.models import Hotel
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic
import datetime
import random

title = "citizi"
test = "Hello, Happy World!"
paragraph = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."

test = {"title" : title, "test" : test, "paragraph" : paragraph}



# Create your views here.
def placeholder(request):
	response = test
	return render(request, "placeholder.html", response)

def crowdTracker(request):
	response = {}
	response["createCrowdTracker"] = CreateCrowdTracker

	return render(request, "crowd-tracker.html", response)

def displayByCity(request):
	response = {'city':'DEBUG'}

	if(request.method == "POST"):
		form = CreateCrowdTracker(request.POST)
		if(form.is_valid()):
			city = form.cleaned_data['city']
			month = form.cleaned_data['month']
			year = form.cleaned_data['year']

			monthTable = {
				1:'January',
				2:'February',
				3:'March',
				4:'April',
				5:'May',
				6:'June',
				7:'July',
				8:'August',
				9:'September',
				10:'October',
				11:'November',
				12:'December'
			}

			isLeapYear = (year%400 == 0 or year%100 != 0) and year%4 == 0

			days_31 = [1, 3, 5, 7, 8, 10, 12]
			days_30 = [4, 6, 9, 11]

			if(month in days_31):
				n = 31
			elif(month in days_30):
				n = 30
			elif(month == 2 and isLeapYear):
				n = 29
			else:
				n = 28

			tracker = CrowdTracker(
				city = city,
				month = month,
				year = year,
				)

			for obj in CrowdTracker.objects.all():
				if(city == obj.city and month == obj.month and year == obj.year):
					obj.delete()
					break

			tracker.save()
			for i in range(n):
				dateVisitor = DateVisitor(
				date = datetime.date(year, month, i+1),
				visitor = 0,
				parent = tracker
				)
				dateVisitor.save()
			updateVisitor(tracker)

			visitorList = [0 for i in range(n)]
			for dateVisitor in DateVisitor.objects.filter(parent=tracker):
				visitorList[dateVisitor.date.day-1] = dateVisitor

			visitorCount = 0
			peakDate = datetime.date(1, 1, 1)
			for visit in visitorList:
				if(visit.visitor >= visitorCount):
					peakDate = visit.date
					visitorCount = visit.visitor

			response['city'] = city
			response['date'] = "{} {}".format(monthTable[month], year)

			response['peak'] = "{} {}".format(peakDate.day, monthTable[month])

			response['visitorList'] = visitorList

	return render(request, "view-crowd.html", response)

def updateVisitor(crowdTracker):
	for visits in Booking.objects.all():
			inDate = visits.check_in.toordinal()
			outDate = visits.check_out.toordinal()
			visitor = visits.guests
			city = visits.hotel.city

			for dateVisitor in DateVisitor.objects.filter(parent=crowdTracker):
				if(inDate <= dateVisitor.date.toordinal() <= outDate and str(city) == str(dateVisitor.parent.city)):
					tmpDate = dateVisitor.date
					tmpVisitor = dateVisitor.visitor + visitor
					obj = DateVisitor(
						date=tmpDate,
						visitor=tmpVisitor,
						parent=crowdTracker
						)
					dateVisitor.delete()
					obj.save()

class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'