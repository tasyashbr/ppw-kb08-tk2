from django.apps import AppConfig


class SearchHotelConfig(AppConfig):
    name = 'search_hotel'
