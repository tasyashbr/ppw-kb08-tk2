$(document).ready(function () {
    searchByCity("Depok");
    $("#searchByCity").change(function () {
        searchByCity(document.getElementById("searchByCity").value);
    });
});

function searchByCity(toSearch) {
    $.ajax({
        url: '/json-data/',
        success: function (results) {
            $("#card").html("");
            for (var i = 0; i < results.length; ++i) {
                var result = results[i];
                if (result.fields.city === toSearch) {
                    $("#card").append(
                        '<a class="hotel-card" href="/book_hotel/' + result.pk + '/">' +
                        '<div id="' + result.pk + '" class="card ml-5 mr-5 mt-4">' +
                        '<div class="card-body"' +
                        '<p>' + result.fields.hotel_name + '</p>' +
                        '<div>' +
                        '<span><img src=' + imgLocation + ' alt="Location" width="20px" height="20px"></span>' +
                        '<span>' + result.fields.address + ', ' + result.fields.city + '</span>' +
                        '</div></div></div></a>'
                    )
                    $("#card-unauth").append(
                        '<a class="hotel-card">' +
                        '<div id="' + result.pk + '" class="card ml-5 mr-5 mt-4">' +
                        '<div class="card-body"' +
                        '<p>' + result.fields.hotel_name + '</p>' +
                        '<div>' +
                        '<span><img src=' + imgLocation + ' alt="Location" width="20px" height="20px"></span>' +
                        '<span>' + result.fields.address + ', ' + result.fields.city + '</span>' +
                        '</div></div></div></a>'
                    )
                }
            }
        },
        error: function () {
            $('<h1>Fetching data failed. Please try again.</h1>').insertAfter("#head");
        }
    });
}