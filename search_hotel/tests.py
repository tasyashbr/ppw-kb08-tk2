from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse

# Create your tests here.


class SearchHotelTest(TestCase):
    def test_booking_page_response(self):
        self.response = Client().get('/booking/')
        self.assertEqual(self.response.status_code, 200)

    def test_booking_url_name(self):
        self.found = resolve('/booking/')
        self.assertEqual(self.found.url_name, 'booking')

    def test_template_doesnt_exist(self):
        self.response = Client().get('/error')
        self.assertEqual(self.response.status_code, 404)

    def test_json_data_response(self):
        self.response = Client().get('/json-data/')
        self.assertEqual(self.response.status_code, 200)
