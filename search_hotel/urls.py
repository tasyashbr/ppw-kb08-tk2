from django.urls import path
from .views import search_hotel, convert_to_json

urlpatterns = [
    path('booking/', search_hotel, name="booking"),
    path('json-data/', convert_to_json, name="json-data"),
]
