from django.shortcuts import render, redirect, HttpResponse
from add_hotel.models import Hotel
from django.core import serializers

# Create your views here.


def convert_to_json(request):
    hotel_json = serializers.serialize("json", Hotel.objects.all())
    print(hotel_json)
    return HttpResponse(hotel_json, content_type='application/json')


def search_hotel(request):
    return render(request, "search_hotel.html")
